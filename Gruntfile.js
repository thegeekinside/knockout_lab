var path = require('path');

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    bower: {
      install: {
         options: {
            targetDir: './lib',
            layout: 'byComponent',
            install: true,
            verbose: true,
            cleanBowerDir: false
        }
      }
    },
    less: {
      development: {
        options: {
          paths: ["bower_components/bootstrap/less"]
        },
        files: {
          "lib/bootstrap/bootstrap.css": "lib/bootstrap/bootstrap.less"
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-bower-task');
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask("default", ['bower']);
};
