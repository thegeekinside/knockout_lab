(function (MainApp) {
  // MonitorItem Constructor
  function MonitorItem (emisora,
                        price,
                        volumen_compra,
                        porcentaje,
                        precio_compra,
                        precio_venta,
                        volumen_venta,
                        volumen_operacion,
                        anterior,
                        apertura,
                        max,
                        min,
                        marketDepth) {
    var self = this;

    self.emisora = emisora;
    self.price = ko.observable(price);
    self.volumen_compra = ko.observable(volumen_compra);
    self.porcentaje = porcentaje;
    self.precio_compra = ko.observable(precio_compra);
    self.precio_venta = ko.observable(precio_venta);
    self.volumen_venta = ko.observable(volumen_venta);
    self.volumen_operacion = ko.observable(volumen_operacion);
    self.anterior = anterior;
    self.apertura = apertura;
    self.max = max;
    self.min = min;
    self.marketDepth = marketDepth;
  }

  // add to the namespace
  MainApp.MonitorItem = MonitorItem;

}(window.MainApp));
