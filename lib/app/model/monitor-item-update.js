(function(MainApp) {

  // Representa un cambio en el precio
  function MonitorItemUpdate(tipoReg,
                         vendedor,
                         horaHecho,
                         volumen,
                         secuencia,
                         ultimoPrecio,
                         tsflmmin,
                         tsflmmout,
                         trans,
                         emisoraSerie,
                         tipoOper,
                         comprador) {
  var self = this;

  self.tipoReg = tipoReg;
  self.vendedor = vendedor;
  self.horaHecho = horaHecho;
  self.volumen = volumen;
  self.secuencia = secuencia;
  self.ultimoPrecio = ultimoPrecio;
  self.tsflmmin = tsflmmin;
  self.tsflmmout = tsflmmout;
  self.trans = trans;
  self.emisoraSerie = emisoraSerie;
  self.tipoOper = tipoOper;
  self.comprador = comprador
  }

  // add to the namespace
  MainApp.MonitorItemUpdate = MonitorItemUpdate;

}(window.MainApp));
