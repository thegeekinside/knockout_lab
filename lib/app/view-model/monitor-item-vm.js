(function (MainApp) {
  function MonitorItemViewModel() {
    var self = this;

    self.selectedMonitorItem = ko.observable();
    self.monitorItemCollection = ko.observableArray([
      new MainApp.MonitorItem('GBM', 10, 12, 4, 5, 7, 18, 17, 45, 65, 15, 18),
      new MainApp.MonitorItem('BIMBO A', 10, 12, 4, 5, 7, 18, 17, 45, 65, 15, 18),
      new MainApp.MonitorItem('ELEKTRA *', 10, 12, 4, 5, 7, 18, 17, 45, 65, 15, 18),
      new MainApp.MonitorItem('ICA *', 10, 12, 4, 5, 7, 18, 17, 45, 65, 15, 18),
      new MainApp.MonitorItem('HOMEX *', 10, 12, 4, 5, 7, 18, 17, 45, 65, 15, 18)
    ]);

    self.addNewItem = function(emisora,
                           price,
                           volumen_compra,
                           porcentaje,
                           precio_compra,
                           precio_venta,
                           volumen_venta,
                           volumen_operacion,
                           anterior,
                           apertura,
                           max,
                           min,
                           marketDepth) {

      var item = new MainApp.MonitorItem(emisora,
                                         price,
                                         volumen_compra,
                                         porcentaje,
                                         precio_compra,
                                         precio_venta,
                                         volumen_venta,
                                         volumen_operacion,
                                         anterior,
                                         apertura,
                                         max,
                                         min,
                                         marketDepth)

      if(self.monitorItemCollection.indexOf(item) > -1) {
        return;
      }

      self.monitorItemCollection.push(item);
      self.selectedMonitorItem(null);
      }

    self.updatePrice = function(p) {

      console.log('Buscando ' + p.emisoraSerie  )
      
      var items = self.monitorItemCollection();
      var item = _.chain(items).filter(function (i) {
                                        return i.emisora == p.emisoraSerie;
                                       }).first().value();
      
      if (item !== null) {
          var t = p.tipoOper == 'CO' ? 'compra' : 'venta';
          
          item.price(p.ultimoPrecio);
          item['volumen_' + t](p.volumen);
          item['precio_' + t](p.ultimoPrecio);        
      }
    }

    self.updateMarketDepth = function(emisora, marketDepth) {
      for (var i=0, len=self.monitorItemCollection().length; i < len; i++){
        if (self.monitorItemCollection()[i].emisora == emisora) {
            self.monitorItemCollection()[i].marketDepth(marketDepth);
        }
      }
    }
  }

  // add to name space
  MainApp.MonitorItemViewModel = MonitorItemViewModel;

} (window.MainApp));
