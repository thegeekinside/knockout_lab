//-- Utils
(function(MainApp) {
  function Utils () {
    var self = this;

    self.dump = function(o) {
      for (var p in o) {
        console.log(p + " : " + o[p]);
      }
    }

    self.random = function(i) {
      return Math.floor(Math.random() * i);
    }
  }

  MainApp.Utils = Utils;
}(window.MainApp));
