// Define the namespace
window.MainApp = {};

(function(MainApp) {
  function App () {
    var self = this;

    self.utils = new MainApp.Utils();
    self.vm = new MainApp.MonitorItemViewModel();

    self.run = function () {
      ko.applyBindings(self.vm);
    }

    self.randomChangePrices = function() {
      var updatePrice = self.vm.updatePrice;

      var emisoras = ['GBM', 'BIMBO A', 'ELEKTRA *', 'ICA *', 'HOMEX *'];
      var precios = _.range(1000, 30000, 1000);
      var tipos = ['CO', 'VE'];
      var volumenes = _.range(0, 30, 5);

      var emisora = _.sample(emisoras);
      var tipo    = _.sample(tipos);
      var volumen = _.sample(volumenes);
      var precio  = _.sample(precios);

      console.log('idx: ' + emisora + ' tipo: ' + tipo + ' volumen: ' + volumen + ' precio: ' + precio);

      updatePrice({'emisoraSerie' : emisora,
                   'tipoOper': tipo,
                   'volumen': volumen,
                   'ultimoPrecio': precio
                  });
    }
  }

  MainApp.App = App;

}(window.MainApp));